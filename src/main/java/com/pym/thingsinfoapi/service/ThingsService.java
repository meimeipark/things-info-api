package com.pym.thingsinfoapi.service;

import com.pym.thingsinfoapi.entity.Things;
import com.pym.thingsinfoapi.model.ThingsCreateRequest;
import com.pym.thingsinfoapi.model.ThingsItem;
import com.pym.thingsinfoapi.model.ThingsResponse;
import com.pym.thingsinfoapi.repository.ThingsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ThingsService {
    private final ThingsRepository thingsRepository;

    public void setThings (ThingsCreateRequest request) {
        Things addData = new Things();
        addData.setThumbNail(request.getThumbNail());
        addData.setName(request.getName());
        addData.setPrice(request.getPrice());

        thingsRepository.save(addData);
    }

    public List<ThingsItem> getThings(){
        List<Things> originList = thingsRepository.findAll();

        List<ThingsItem> result = new LinkedList<>();

        for (Things things: originList){
            ThingsItem addItem = new ThingsItem();
            addItem.setId(things.getId());
            addItem.setThumbNail(things.getThumbNail());
            addItem.setName(things.getName());
            addItem.setPrice(things.getPrice());

            result.add(addItem);
        }
        return result;
    }

    public ThingsResponse getThing(long id) {
        Things originData = thingsRepository.findById(id).orElseThrow();
        ThingsResponse response = new ThingsResponse();
        response.setId(originData.getId());
        response.setThumbNail(originData.getThumbNail());
        response.setName(originData.getName());
        response.setPrice(originData.getPrice());

        return response;
    }
}
