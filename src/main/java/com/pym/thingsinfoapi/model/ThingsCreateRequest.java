package com.pym.thingsinfoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ThingsCreateRequest {
    private String thumbNail;
    private String name;
    private Double price;
}
