package com.pym.thingsinfoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ThingsResponse {
    private Long id;
    private String thumbNail;
    private String name;
    private Double price;
}
