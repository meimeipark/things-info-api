package com.pym.thingsinfoapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter

// CommonResult의 변수 상속(extends)
// -> 상속을 받을 경우 회 시 CommonResult에 있는 변수 인 msg, code도 추가된다.
public class ListResult<T> extends CommonResult{
    private List<T> list;
    private Integer totalCount;
}
