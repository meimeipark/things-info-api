package com.pym.thingsinfoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

// 공통적 인사
// code에서 의미하는 숫자 0: 정상, -음수로 나오는 숫자는 에러 코드
public class CommonResult {
    private String msg;
    private Integer code;
}
