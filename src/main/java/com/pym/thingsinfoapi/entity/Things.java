package com.pym.thingsinfoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Things {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(nullable = false)
    private String thumbNail;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double price;
}
