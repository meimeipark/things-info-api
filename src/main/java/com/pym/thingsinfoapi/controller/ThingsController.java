package com.pym.thingsinfoapi.controller;

import com.pym.thingsinfoapi.entity.Things;
import com.pym.thingsinfoapi.model.ListResult;
import com.pym.thingsinfoapi.model.ThingsCreateRequest;
import com.pym.thingsinfoapi.model.ThingsItem;
import com.pym.thingsinfoapi.model.ThingsResponse;
import com.pym.thingsinfoapi.service.ThingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("v1/things")
public class ThingsController {
    private final ThingsService thingsService;

    @PostMapping("/new")
    public String setThings(@RequestBody ThingsCreateRequest request){
        thingsService.setThings(request);

        return "OK";
    }

    @GetMapping("/all")
    public ListResult<ThingsItem> getThings(){
        List<ThingsItem> list = thingsService.getThings();

        ListResult<ThingsItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    @GetMapping("/detail/{id}")
    public ThingsResponse getThing(@PathVariable long id){
        return thingsService.getThing(id);
    }
}
