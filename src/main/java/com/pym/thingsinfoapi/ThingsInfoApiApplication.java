package com.pym.thingsinfoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThingsInfoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThingsInfoApiApplication.class, args);
    }

}
