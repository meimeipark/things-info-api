package com.pym.thingsinfoapi.repository;

import com.pym.thingsinfoapi.entity.Things;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ThingsRepository extends JpaRepository <Things, Long> {
}
